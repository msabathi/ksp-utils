{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Scanning orbit calculator\n",
    "\n",
    "The goal of this notebook is to compute the best polar circular orbit altitude in order to scan the totality of a body's\n",
    "surface according to the instrument's field of view ($FOV$).\n",
    "\n",
    "## Calculations\n",
    "\n",
    "First we find $\\theta$ as defined by the scheme below\n",
    "\n",
    "![scanning-scheme.png](./scanning-scheme.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\displaystyle \\tan{\\left(\\frac{FOV}{2} \\right)} = \\frac{R \\sin{\\left(\\frac{\\theta}{2} \\right)}}{R \\left(1 - \\cos{\\left(\\frac{\\theta}{2} \\right)}\\right) + h}$"
      ],
      "text/plain": [
       "Eq(tan(FOV/2), R*sin(theta/2)/(R*(1 - cos(theta/2)) + h))"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/latex": [
       "$\\displaystyle \\theta = 4 \\operatorname{atan}{\\left(\\frac{R - \\sqrt{R^{2} - 2 R h \\tan^{2}{\\left(\\frac{FOV}{2} \\right)} - h^{2} \\tan^{2}{\\left(\\frac{FOV}{2} \\right)}}}{\\left(2 R + h\\right) \\tan{\\left(\\frac{FOV}{2} \\right)}} \\right)}$"
      ],
      "text/plain": [
       "Eq(theta, 4*atan((R - sqrt(R**2 - 2*R*h*tan(FOV/2)**2 - h**2*tan(FOV/2)**2))/((2*R + h)*tan(FOV/2))))"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "\n",
    "import sympy as sp\n",
    "\n",
    "FOV, theta, h, R = sp.symbols('FOV theta h R')\n",
    "eq_fov = sp.Eq(sp.tan(FOV/2), R*sp.sin(theta/2)/(h+R*(1-sp.cos(theta/2))))\n",
    "display(eq_fov)\n",
    "\n",
    "sol_theta = sp.solve(eq_fov, theta)[0]\n",
    "theta_eq = sp.Eq(theta, sol_theta)\n",
    "display(theta_eq)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then we need to find the altitude $h$ at which the angle traveled by the body over an orbital period $T$ is a multiple\n",
    "of $\\theta$ so that we don't overlap an already scanned area.\n",
    "\n",
    "We express the traveled angle as a multiple of theta in the synchronicity equation by"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\displaystyle \\frac{2 \\pi T}{T_{rev}} = k \\theta$"
      ],
      "text/plain": [
       "Eq(2*pi*T/T_rev, k*theta)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "T, T_rev, Omega, k = sp.symbols('T T_rev Omega k')\n",
    "sync_eq = sp.Eq(2*sp.pi*T/T_rev, k*theta)\n",
    "display(sync_eq)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "with $k$ being an integer and $T_{rev}$ the revolution period of the body.\n",
    "\n",
    "The third Kepler law gives us the orbital period $T$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\displaystyle T = 2 \\pi \\sqrt{\\frac{\\left(R + h\\right)^{3}}{G M}}$"
      ],
      "text/plain": [
       "Eq(T, 2*pi*sqrt((R + h)**3/(G*M)))"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "G, M = sp.symbols('G M')\n",
    "T_eq = sp.Eq(T, 2*sp.pi*sp.sqrt((R+h)**3/(G*M)))\n",
    "display(T_eq)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "with $G$ the gravitational constant and $M$ the mass of the body.\n",
    "\n",
    "Replacing $T$ and $\\theta$ in the synchronicity equation yields"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\displaystyle \\frac{4 \\pi^{2} \\sqrt{\\frac{\\left(R + h\\right)^{3}}{G M}}}{T_{rev}} = 4 k \\operatorname{atan}{\\left(\\frac{R - \\sqrt{R^{2} - 2 R h \\tan^{2}{\\left(\\frac{FOV}{2} \\right)} - h^{2} \\tan^{2}{\\left(\\frac{FOV}{2} \\right)}}}{\\left(2 R + h\\right) \\tan{\\left(\\frac{FOV}{2} \\right)}} \\right)}$"
      ],
      "text/plain": [
       "Eq(4*pi**2*sqrt((R + h)**3/(G*M))/T_rev, 4*k*atan((R - sqrt(R**2 - 2*R*h*tan(FOV/2)**2 - h**2*tan(FOV/2)**2))/((2*R + h)*tan(FOV/2))))"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "sync_eq = sync_eq.subs(T_eq.lhs, T_eq.rhs).subs(theta_eq.lhs, theta_eq.rhs)\n",
    "display(sync_eq)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Solving this equation analytically is too complex for `sympy` so we'll solve it numerically using `sympy.nsolve`.\n",
    "\n",
    "## Numerical solution"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "M_mun = 9.76e20\n",
    "R_mun = 200000\n",
    "T_rev_mun = 138984.38"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'k=1': 'No solution found',\n",
       " 'k=2': 'No solution found',\n",
       " 'k=3': 'No solution found',\n",
       " 'k=4': 140787,\n",
       " 'k=5': 87131,\n",
       " 'k=6': 64034,\n",
       " 'k=7': 50821,\n",
       " 'k=8': 42196,\n",
       " 'k=9': 36102,\n",
       " 'k=10': 31559}"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "values = {G:6.67408e-11,\n",
    "          R:R_mun,\n",
    "          M:M_mun,\n",
    "          T_rev:T_rev_mun,\n",
    "          FOV:4.5*(sp.pi/180),\n",
    "          k:1}\n",
    "\n",
    "h_min = 14000\n",
    "h_max = 500000\n",
    "\n",
    "h_sol={}\n",
    "for i in range(1,11):\n",
    "    values[k] = i\n",
    "    sols = []\n",
    "    for h0 in range(h_min, h_max, 10000):\n",
    "        try:\n",
    "            sol = round(sp.nsolve(sync_eq.subs(values), h0)) # type: ignore\n",
    "            if sol not in sols and 'I' not in str(sol): # Filter redundant and complex solutions\n",
    "                sols.append(sol)\n",
    "        except:\n",
    "            pass\n",
    "    if not sols:\n",
    "        h_sol['k='+str(i)] = 'No solution found'\n",
    "    elif len(sols)==1:\n",
    "        h_sol['k='+str(i)] = sols[0]\n",
    "    else:\n",
    "        h_sol['k='+str(i)] = sols\n",
    "display(h_sol)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "dev",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.0"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
