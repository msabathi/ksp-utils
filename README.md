# KSP Utils

## Description
Here are some Kerbal Space Program utilities i use to optimize vessel trajectory and compute orbit parameters.

## Usage
Use Jupyter notebooks to read and execute the files.

## Contributing
The repository is open for contributions of all types.

## License
Released under the [GNU General Public License version 3](./LICENSE.md)
